#coding: utf-8

from fabric.api import env, local, run, put

"""
Comandos de administração
"""

env.hosts = ['bitnami@192.168.56.101']

def teste():
    "Simples método de teste"
    print "ok"

def listar():
    result = local('dir', capture=True)
    if result.succeeded:
        for l in result.splitlines():
            print 'saida: ', l.upper()


def remoto():
    run('ls -l')


def enviar():
    _upload_file('hello.py', '/home/bitnami/hello.py')
    run('python hello.py')


def provisionar():
    run('sudo apt-get install git')


def _upload_file(orig, dest):
    put(orig, dest, use_sudo=True)